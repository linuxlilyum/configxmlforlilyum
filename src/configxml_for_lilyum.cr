# TODO: Write documentation for `ConfigxmlForLilyum`
require "option_parser"

require "./*"
module ConfigxmlForLilyum
  VERSION = "0.1.0"
  working_path = Dir.current
  ##Default settings for this program
  #The variables which starts with "informaton_" will placed in header of config.xml
  information_project="Lilyum"
  information_name="The Lilyum Project"
  information_specification="Lilyum project is an Linux distribution which based on OpenSUSE. Lilyum's primary goal are elegancy and Performance"
  information_contact="şirinler@samsunlular.com (This is an sample, not a real email adress)"
  information_version="0.55"

  #The variables which starts with "preferences_" contains configuration of image
  preferences_kernel_parameters=["quiet"]
  #Turkish: tr_TR
  ##American English: en_US
  preferences_locale="tr_TR"
  #Turkish Q: trq.map.gz
  #Turkish F: trf.map.gz
  #American: us.map.gz
  preferences_keytable="trq.map.gz"
  preferences_timezone="Europe/Istanbul"

  default_desktop="kdeplasma" #Only minimal, gnome, kdeplasma and i3 is valid
  #This section must be declared as Array and the items in Array must match the names of packages
  default_browser=["chromium"]
  default_email=["MozillaThunderbird"]
  default_multimedia=["elisa","kaffeine"]
  default_pictures=["krita","gwenview5"]
  default_games_n_communication=["steam","telegram-desktop"]
  default_others=["kget"]

  #However, the variables in the below should be String
  default_graphics="amd"
  #Default shell would not changed via this line. "chsh" command should be run by config.sh
  default_shell="fish"
  kiwi_key = false
  kiwi_log = false


  output_path = "./"

  OptionParser.parse do |parser|
    parser.banner = "Config.xml Creator For Lilyum"
    parser.on "-v","--version","Show Version" do
      puts VERSION
      exit
    end
    parser.on "-p PROJECT","--project=PROJECT","Set the project name" do |project|
      information_project=project
    end
    parser.on "-n NAME","--name=NAME","Set the name of developer" do |name|
      information_name=name
    end
    parser.on "-c CONTACT","--contact=CONTACT","Set the contact e-mail of developer" do |contact|
      information_contact=contact
    end
    parser.on "-v VERSION","--version=VERSION","Set the version of project" do |version|
      information_version=version
    end
    parser.on "-s SPECIFICATION","--specification=SPECIFICATION","Set the specification of project" do |specification|
      information_specification=specification
    end
    parser.on "-kp KERNEL_PARAMETERS","--kernel_parameter=KERNEL_PARAMETERS","Specify kernel parameters. It can be listed with commas. e.g: 'browser1,browser2" do |kernel_parameters|
      preferences_kernel_parameters = kernel_parameters.split(",") << "slient"
    end
    parser.on "-l LOCALE","--locale=LOCALE","Set the locale of image. e.g: en_US (American English) or tr_TR (Turkish)" do |locale|
      preferences_locale = locale
    end
    parser.on "-k KEYTABLE","--keytable=KEYTABLE","Set the keytable set of image. e.g: us.map.gz (American), trq.map.gz (Turkish Q), trf (Turkish F)" do |keytable|
      preferences_keytable = keytable
    end
    parser.on "-t TIMEZONE","--timezone=TIMEZONE","Set the timezone of image. e.g: Europe/Istanbul or Asia/Nicosia" do |timezone|
      preferences_timezone = timezone
    end
    parser.on "-ds DESKTOP","--desktop=DESKTOP","Set the desktop of image. Only kdeplasma, i3 and gnome is valid" do |desktop|
      default_desktop = desktop
    end
    parser.on "-wb BROWSERS","--web_browser=BROWSERS","Set the web browsers will be installed in the system. It can be listed with commas. e.g: 'browser1,browser2'" do |browsers|
      default_browser = browsers.split(",")
    end
    parser.on "-ec EMAIL_CLIENTS","--email_clients=EMAIL_CLIENTS","Set the email clients will be installed in the system. It can be listed with commas. e.g: 'mailclient1,mailclient2'" do |email_clients|
      default_email = email_clients.split(",")
    end
    parser.on "-mm MULTIMEDIA","--multimedia=MULTIMEDIA","Set the multimedia programs will be installed in the system. It can be listed with commas. e.g: 'multimedia1,multimedia2'" do |multimedia|
      default_multimedia = multimedia.split(",")
    end
    parser.on "-pc PICTURES","--pictures=PICTURES","Set the picture viewers and editors will be installed in the system. It can be listed with commas. e.g: 'picture1,picture2'" do |pictures|
      default_pictures = pictures.split(",")
    end
    parser.on "-gr GRAPHICS","--graphics=GRAPHICS","Set the graphic packages of system. 'nvidia' for Nvidia, 'amd' for AMD. You can choose 'amd' for Intel onboard systems" do |graphics|
      default_graphics = graphics
    end
    parser.on "-ot","--others","Set the other packages will be installed in the system. Currently, #{default_others} in this category. It can be listed with commas. e.g 'package1,package2'" do |others|
      default_others = others.split(",")
    end
    parser.on "-sh SHELL","--shell=SHELL","Set the package name of Shell. It would be installed, but not be set unless you command 'chsh' in the config.sh" do |shell|
      default_shell = shell
    end
    parser.on "-r","--run","Run Kiwi after the 'Config.xml' created." do |condition|
      kiwi_key = true
      kiwi_log = condition.downcase ? true : false
    end
    parser.invalid_option do |flag|
      STDERR.puts "ERROR: #{flag} Invalid Flag"
      STDERR.puts parser
      exit(1)
    end
    parser.on "-h","--help","Show help" do
      puts parser
      exit
    end
  end

  config_xml = File.new "#{working_path}/config.xml", "w"
  header_content = Header.new information_project, information_name, information_contact, information_specification, preferences_kernel_parameters, information_version, preferences_locale, preferences_keytable, preferences_timezone
  middle_content = Repositories.new 
  footer_content = Packages.new default_desktop, default_browser, default_email, default_multimedia, default_pictures, default_games_n_communication, default_others, default_graphics, default_shell
  place_to_config_xml = %(#{header_content}
    #{middle_content}
    <packages type="bootstrap">
      #{footer_content}
    </packages>
</image>
)
  config_xml.puts(place_to_config_xml).to_s
  config_xml.close

  if kiwi_key
    puts "Kiwi initalized! Sudo will ask password if you're not root"
    sleep(1)
    system "mkdir output"
    system "LANG=C sudo kiwi-ng --type iso system build --description #{working_path} --target-dir #{working_path}/output"
  end
end
