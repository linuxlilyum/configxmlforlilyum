module Header
  def Header.new(project,name,contact,specification,kernelprm,version,locale,keytable,timezone)
    kernelprm = kernelprm.join(",")
    return %(<?xml version="1.0" encoding="utf-8"?>

  <image schemaversion="6.8" name="#{project}">
     <description type="system">
          <author>#{name}</author>
          <contact>#{contact}</contact>
          <specification>
              #{specification}
          </specification>
      </description>
      <preferences>
          <type image="iso" primary="true" flags="overlay" squashfscompression="zstd" hybrid="true" firmware="uefi" kernelcmdline="#{kernelprm}" hybridpersistent_filesystem="xfs" hybridpersistent="true" mediacheck="true"/>
          <version>#{version}</version>
          <packagemanager>zypper</packagemanager>
          <locale>#{locale}</locale>
          <keytable>#{keytable}</keytable>
          <timezone>#{timezone}</timezone>
          <rpm-excludedocs>true</rpm-excludedocs>
          <rpm-check-signatures>false</rpm-check-signatures>
          <bootsplash-theme></bootsplash-theme>
          <bootloader-theme>Lilyum</bootloader-theme>
      </preferences>
      <users>
          <user password="" home="/root" name="root" groups="root"/>
      </users>
    )
  end
end
