module Packages
  Packages::BASE=["checkmedia","patterns-openSUSE-base",
        "iputils","vim","grub2",
        "grub2-x86_64-efi","syslinux","lvm2",
        "fontconfig","fonts-config",
        "tar","parted", "openssh", "iproute2",
        "less","bash-completion","dhcp-client",
        "which","shim","kernel-default","timezone",
        "dracut-kiwi-live","udev","filesystem",
        "glibc-locale","openSUSE-release","cracklib-dict-full",
        "kexec-tools","kernel-default","zypper","kernel-firmware",
        "grub","calamares","yast2-firstboot",
        "libmariadb3","libQt5Sql5-mysql"]
  #garnitur is pronounciation of garniture in Turkish.
  #This is just a joke for Turkish people. You can think that as "Additional Packages to Base System"
  Packages::GARNITUR=["ca-certificates","ca-certificates-mozilla","mokutil",
        "usbmuxd","haveged","dosfstools","udisks2",
        "ntp","cryptsetup","grub2-snapper-plugin",
        "coreutils","libiptcdata0","libiptcdata","libibus-1_0-5",
        "glibc-locale","glibc","pulseaudio","alsa-plugins-pulse",
        "pulseaudio-utils","pulseaudio-module-bluetooth","pulseaudio-module-zeroconf",
        "pulseaudio-module-gconf","avahi","alsa-firmware","libstdc++6-32bit",
        "ModemManager","os-prober","gpg2","extra-cmake-modules","rpcbind",
        "ucode-amd","ucode-intel"]
  Packages::HARDWARE_ETC=["crda","Mesa-dri","libva2","libva-x11-2",
          "libva-drm2","system-config-printer",
          "libinput-tools","libinput-udev","bcm43xx-firmware","bcm20702a1-firmware",
          "gvfs-backend-samba","lvm2","libmtp9",
          "hfsutils","jfsutils","xfsprogs","ntfs-3g","ntfsprogs",
          "mtp-tools","sane-backends-autoconfig","samba","ifuse",
          "imobiledevice-tools",
          "cups-filters-cups-browsed","cups-backends",
          "parallel-printer-support","udev-configure-printer",
          "yast2-printer","xf86-video-fbdev","libglut3",
          "virtualbox-guest-tools","tlp-rdw","tlp","iw",
          "NetworkManager","xorg-x11-libX11-ccache","xorg-x11-libs",
          "xorg-x11-driver-input","yast2-x11","xorg-x11-server-extra",
          "yast2-fonts","fuse-exfat","gvfs-backend-afc","gvfs-backends",
          "Mesa-demo-x","exfat-utils","libxcb-xf86dri0","iputils",
          "e2fsprogs","sax3","xf86-input-keyboard","xf86-video-vmware",
          "xf86-input-mouse","xf86-input-synaptics","xf86-input-vmmouse",
          "xf86-input-evdev","xf86-input-wacom","xf86-input-joystick",
          "gutenprint","epson-inkjet-printer-escpr","cups-pk-helper",
          "cups-filters","manufacturer-PPDs","OpenPrintingPPDs",
          "OpenPrintingPPDs-ghostscript","OpenPrintingPPDs-postscript",
          "OpenPrintingPPDs-hpijs","bluez-firmware","ivtv-firmware",
          "bcm43xx-firmware","atmel-firmware","mpt-firmware",
          "adaptec-firmware","zd1211-firmware","crystalhd-firmware",
          "isight-firmware-tools","ipw-firmware","xf86-video-intel",
          "intel-media-driver","xorg-x11","usb_modeswitch",
          "system-config-printer-dbus-service"]

  Packages::ADDITIONAL=["p7zip","unrar","python3-smbc","gparted","python-gtk",
              "python3-coverage","gdk-pixbuf-loader-rsvg",
              "samba-winbind","samba","hunspell","poppler-tools","Mesa",
              "sudo","nano","libdmx1","htop","curl","gsettings-backend-dconf",
              "vim","neovim","xdg-user-dirs","xdg-user-dirs-gtk","gvfs","gvfs-fuse",
              "rsync","psmisc","xkill","xrandr","vlc-codecs","wget","rar",
              "unrar","git","flatpak","neofetch","fftw3-devel",
              "linux-glibc-devel","linux-glibc-devel"]

  Packages::SUSEUTIL=["patterns-openSUSE-minimal_base","patterns-openSUSE-print_server",
            "patterns-yast-x11_yast", "patterns-openSUSE-sw_management_gnome",
            "patterns-openSUSE-gnome_yast","patterns-openSUSE-enhanced_base",
            "patterns-fonts-fonts","patterns-yast-yast2_install_wf", "patterns-fonts-fonts_opt",
            "patterns-openSUSE-sw_management","patterns-yast-yast2_basis",
            "patterns-openSUSE-x11","patterns-yast-yast2_basis","yast2-printer",
            "yast2-snapper","snapper-zypp-plugin","yast2-sudo",
            "yast2-metapackage-handler","yast2-samba-client","yast2-samba-server"]

  Packages::KDEPLASMA=["libqt5-qtstyleplugins-platformtheme-gtk2",
             "libqt5-qtbase-platformtheme-gtk3","kdnssd",
             "kio-extras5","spectacle",
             "baloo5-file","baloo5-tools","kdegraphics-thumbnailers",
             "kde-odf-thumbnail","plasma5-addons","plasma5-pa","bluedevil5",
             "dolphin","kate","kcalc","kcm-touchpad","kde-gtk-config5",
             "kde-print-manager","kio_audiocd","kio_iso","kio_kamera",
             "konsole","kscreen","patterns-kde-kde_yast","okular","plasma5-session",
             "skanlite","kfind","phonon4qt5-backend-gstreamer","ark",
             "latte-dock","kvantum-qt5","kcoreaddons-devel","kguiaddons-devel","kconfig-devel",
             "kwindowsystem-devel","ki18n-devel","kconfigwidgets-devel","libQt5DBus-devel",
             "libqt5-qtx11extras-devel","flat-remix-icon-theme","sddm","yakuake"]

  Packages::I3GROUP=["galculator","rofi","i3","dmenu","i3status","i3blocks",
           "NetworkManager-gnome","conky","jgmenu","imagemagick",
           "python-gobject2","python-gobject","python-pip","arandr",
           "lxappearance","pavucontrol","zsh","lightdm"]

  Packages::GNOME=["gnome-terminal","gnome-disk-utility","gedit","gedit-plugins","gnome-screenshot",
                "gnome-photos","libreoffice-gnome","libreoffice-gtk3","gnome-system-monitor","gdm-branding-openSUSE",
                "gnome-tweak-tool","gnome-shell-search-provider-documents","gnome-shell-search-provider-contacts",
                "gnome-shell-search-provider-nautilus","gnome-shell-search-provider-gnome-terminal","gnome-shell-extension-gpaste",
                "gnome-shell-search-provider-gnome-calculator","gnome-power-manager","gnome-keyring",
                "gnome-control-center-user-faces","NetworkManager-gnome","nautilus-extension-terminal",
                "nautilus-evince","nautilus-terminal","nautilus-share","gnome-shell-extensions-common",
                "libgnomesu","libgnome-menu-3-0","gnome-control-center-goa","gnome-session-wayland",
                "python-gtk","libqt5-qtstyleplugins-platformtheme-gtk2","python3-gobject-Gdk"]

  Packages::CODECS_N_FONTS=["gstreamer-plugins-good-extra","gstreamer-plugins-bad","cantarell-fonts",
                "google-droid-fonts","dejavu-fonts","ubuntu-fonts","libavcodec56","libavcodec57",
                "libavcodec58","libavformat56","libavformat57","libavformat58","libavdevice56",
                "libavdevice57","libavdevice58","noto-coloremoji-fonts"]
  Packages::LIBREOFFICE=["libreoffice","libreoffice-calc","libreoffice-draw","libreoffice-impress","libreoffice-writer","libreoffice-gtk3"]
  Packages::BRANDING=["calamares-lilyum"]
  Packages::FIREFOXUTIL=["kmozillahelper"]
  Packages::CHROMIUMUTIL=["chromium-ffmpeg-extra","chromedriver"]
  Packages::NVIDIAUTIL=["suse-prime","x11-video-nvidiaG05"]
  Packages::AMDUTIL=["xf86-video-amdgpu"]

  def Packages.gpu(graphics)
    case graphics.to_s.downcase
    when "nvidia"
      return Packages::NVIDIAUTIL
    when "amd"
      return Packages::AMDUTIL
    else
      [] of String
    end
  end

  def Packages.preferred_desktop(desktop)
    case desktop.to_s.downcase
    when "kdeplasma"
      return Packages::KDEPLASMA
    when "i3"
      return Packages::I3GROUP
    when "gnome"
      return Packages::GNOME
    else
      return [] of String
    end
  end

  def Packages.browser_utilities(*args, desktop="other")
    list_to_return=[] of String
    args.each do |choise|
      case choise.to_s.downcase
      when "mozillafirefox"
        if desktop == "kdeplasma"
          list_to_return = list_to_return + Packages::FIREFOXUTIL
        end
      when "chromium", "chrome"
        list_to_return = list_to_return + Packages::CHROMIUMUTIL
      end
    end
    return list_to_return
  end

  def Packages.format(name)
    return "<package name='#{name}'/>"
  end

  def Packages.new(desktop,browser,email,multimedia,pictures,communication,others,graphics,shell)
    #For the readability of code, I declared the "list_of_packages" three_time. I know this is a bad idea, but no one will get harm :)
    list_of_packages = (Packages.gpu(graphics) + Packages.preferred_desktop(desktop) + Packages.browser_utilities(browser,desktop=desktop.to_s))
    #For the choises of User
    list_of_packages = (list_of_packages + browser + email + multimedia + pictures + others) << shell
    #For same packages will used for all Lilyum settings
    list_of_packages = list_of_packages + Packages::BASE + Packages::GARNITUR + Packages::HARDWARE_ETC + Packages::ADDITIONAL + Packages::SUSEUTIL + Packages::BRANDING + Packages::LIBREOFFICE
    list_of_packages.map! do |package|
      package = Packages.format(package)
    end
    return_item = list_of_packages.join "\n\t\t\t"
    return return_item
  end
end
