module Repositories
  def Repositories.template(name,source,priority)
    %(\t<repository type='rpm-md' alias="#{name}" imageinclude="true" package_gpgcheck="false" repository_gpgcheck="false" priority="#{priority}">
        <source path='#{source}'/>
      </repository>)
  end
  def Repositories.new()
    sources={"Packman" => ["http://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/",90],
         "Tarbetu"=>["https://download.opensuse.org/repositories/home:/Tarbetu/openSUSE_Tumbleweed/",82],
         "Wine"=>["https://download.opensuse.org/repositories/Emulators:/Wine/openSUSE_Tumbleweed/",91],
         "KDE:Extra"=>["http://download.opensuse.org/repositories/KDE:/Extra/openSUSE_Tumbleweed/",91],
         "YaST:Head"=>["http://download.opensuse.org/repositories/YaST:/Head/openSUSE_Tumbleweed/",91],
         "Games:Tools"=>["http://download.opensuse.org/repositories/games/openSUSE_Tumbleweed/",92],
         "GoogleChrome"=>["http://dl.google.com/linux/chrome/rpm/stable/x86_64/",98],
         "Signal"=>["https://download.opensuse.org/repositories/network:im:signal/openSUSE_Tumbleweed/",98],
         "Nvidia"=>["https://download.nvidia.com/opensuse/tumbleweed/",89],
         "OpenSUSE_OSS"=>["https://download.opensuse.org/tumbleweed/repo/oss/",99],
         "OpenSUSE_Non-OSS"=>["https://download.opensuse.org/tumbleweed/repo/non-oss/",99],
         "OpenSUSE_Update"=>["https://download.opensuse.org/update/tumbleweed",99],
         }
    list = [] of String
    sources.each do |name,the_tuple|
      list << Repositories.template(name,the_tuple[0],the_tuple[1])
    end
    return list.join "\n\t\t"
  end
end
